package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
        try {
			this.propertyRepository.addProperty(new Property(1, "17 Charnwood Crescent, St.Kilda VIC 3182", 3, 670, new BigDecimal("170000")));
			this.propertyRepository.addProperty(new Property(2, "5 Irwin Street, Clayton VIC 3123", 4, 900, new BigDecimal("140000")));
			this.propertyRepository.addProperty(new Property(3, "153 City Road, SouthBank VIC 3006", 2, 800, new BigDecimal("785000")));
			this.propertyRepository.addProperty(new Property(4, "555 St. Kilda Road, Melbourne VIC 3004", 2, 987, new BigDecimal("765900")));
			this.propertyRepository.addProperty(new Property(5, "12 Queens Road, Melbourne VIC 3004", 1, 230, new BigDecimal("95000")));
			System.out.println("Properties added succesfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("There are no properties in the propertyList to add");
		}
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
        List<Property> propertyList;
		try {
			propertyList = propertyRepository.getAllProperties();
			for (Property property : propertyList) {
	        	System.out.println(property.toString());
	        	}
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Enter the property ID of the property you want to search");
    	int pid = in.nextInt();
    	try{
    		Property property = this.propertyRepository.searchPropertyById(pid);
    		if(property != null) {
    			System.out.println(property.toString());
    	}
    	else
    	{
    		System.out.println("The Property does not exist");
    	}
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
 
   	}
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
